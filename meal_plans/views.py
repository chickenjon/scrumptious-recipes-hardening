from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth import get_user_model
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from meal_plans.models import MealPlan


# Create your views here.


class MealPlanListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    paginate_by = 16
    context_object_name = "meal_plans"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    fields = ["name", "date", "recipes"]

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("meal_plan_detail", kwargs={"pk": self.object.pk})


class MealPlanDetailView(UserPassesTestMixin, DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"
    context_object_name = "plan"

    def test_func(self):
        return self.request.user == self.get_object().owner


class MealPlanUpdateView(UserPassesTestMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = ["name", "date", "recipes"]

    def test_func(self):
        return self.request.user == self.get_object().owner

    def get_success_url(self):
        return reverse_lazy("meal_plan_detail", kwargs={"pk": self.object.pk})


class MealPlanDeleteView(UserPassesTestMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plan_list")
    context_object_name = "plan"

    def test_func(self):
        return self.request.user == self.get_object().owner
