from types import NoneType
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from recipes.models import FoodItem, Recipe, ShoppingItem, Ingredient
from recipes.forms import RatingForm


# def list_users(request):
#     users = get_user_model().all()
#     context = {"user": users}
#     return render(request, "recipes/users.html", context)


def log_rating(request, recipe_id):
    try:
        if request.method == "POST":
            form = RatingForm(request.POST)
            if form.is_valid():
                rating = form.save(commit=False)
                rating.recipe = Recipe.objects.get(pk=recipe_id)
                rating.save()
        return redirect("recipe_detail", pk=recipe_id)
    except Recipe.DoesNotExist:
        return redirect("recipes_list")


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 16


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    # Allow detail view to handle post requests
    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Adding the rating form to the context
        context["rating_form"] = RatingForm()

        # Adding the list of items already in cart to the context
        food_list = []

        for item in self.request.user.shopping_items.all():
            food_list.append(item.food_item)

        context["food_list"] = food_list
        # Adding the serving size to the context

        # If view was reached by submitting the sizing
        # form on the page, use that size value
        if self.request.method == "POST":
            servings = self.request.POST.get("serving_size")

        # Else, use the default serving size of the recipe
        else:
            servings = self.object.servings

        # Add to context
        context["servings"] = servings

        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "servings", "description", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "servings", "description", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


class UserListView(ListView):
    model = get_user_model()
    template_name = "recipes/users.html"
    paginate_by = 16


class ShoppingItemsList(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "cart/list.html"
    context_object_name = "cart"

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)


@login_required(login_url="/login")
def create_shopping_item(request):
    if request.method == "POST":

        ingredient_id = request.POST.get("ingredient_id")
        ingredient = Ingredient.objects.get(id=ingredient_id)

        try:
            ShoppingItem.objects.create(
                food_item=ingredient.food, user=request.user
            )
        except IntegrityError:
            pass

        return redirect("recipe_detail", pk=ingredient.recipe.id)


@login_required(login_url="/login")
def delete_cart(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    return redirect("shopping_items_list")


# class RecipeDetailView(DetailView):
#     model = Recipe
#     template_name = "recipes/detail.html"

#     # Allow detail view to handle post requests
#     def post(self, request, *args, **kwargs):
#         return self.get(request, *args, **kwargs)

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)

#         # Adding the rating form to the context
#         context["rating_form"] = RatingForm()

#         # Adding the list of items already in cart to the context
#         food_list = []

#         for item in self.request.user.shopping_items.all():
#             food_list.append(item.food_item)

#         context["food_list"] = food_list

#         # Adding the serving size to the context

#         # If view was reached by submitting the sizing
#         # form on the page, use that size value
#         if self.request.GET.get("serving_size"):
#             servings = self.request.GET.get("serving_size")

#         # Else, use the default serving size of the recipe
#         else:
#             servings = self.object.servings

#         # Add to context
#         context["servings"] = servings

#         return context
