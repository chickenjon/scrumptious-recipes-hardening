from django import template

register = template.Library()


def resize_to(ingredient, desired_servings):
    default_servings = ingredient.recipe.servings
    if default_servings is not None and desired_servings is not None:
        ratio = int(desired_servings) / default_servings
        return ratio * ingredient.amount
    return ingredient.amount


register.filter("resize_to", resize_to)
